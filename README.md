# Traveller Dungeondraft

Author: Samuel Penn (sam@notasnark.net)
License: CAL-NR

These map assets are for use with Dungeondraft, and provide tiles, walls,
objects and other elements for use in creating maps for a SciFi RPG.

They are specifically designed for use with Traveller, but should be useful
for any similar style of RPG.

They include:
* Walls, mostly for ship hulls. A different wall type is provided for each
of the different 'armour' types.
* Portals, interior doors and airlocks.
* Tiles, for shipboard floor types.
* Paths, for floor markings.
* Objects, for bridge, engineering and living quarters. Also markings for
labels and lettering.

How to define all of these is described here:
https://github.com/Megasploot/Dungeondraft/wiki/Custom-Assets-Guide


## Walls

Wall thickness

Very Thick: 128
Thick: 96
Medium: 64
Thin: 48
Very Thin: 32


## Building

Running `install.sh` will convert the GIMP XCF files and SVG files into
PNGs for use in Dungeoncraft. It will also use `dungeondraft-pack` to
build the asset pack.

If you don't have `dungeondraft-pack`, then you can build the asset pack
manually from within the Dungeondraft GUI.

For the packager, see here:
https://github.com/Ryex/Dungeondraft-GoPackager

